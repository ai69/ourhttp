package helper

import (
	"context"
	"errors"
	"net"
	"net/http"
	"net/url"
	"strings"
)

// IsTimeout returns a boolean indicating whether the error is known to report that a timeout occurred.
func IsTimeout(err error) bool {
	if errors.Is(err, context.DeadlineExceeded) {
		return true
	}

	switch err := err.(type) {
	case *url.Error:
		if err, ok := err.Err.(net.Error); ok && err.Timeout() {
			return true
		}
	case *net.OpError:
		if err.Timeout() {
			return true
		}
	case net.Error:
		if err.Timeout() {
			return true
		}
	}

	if err != nil && strings.Contains(err.Error(), "use of closed network connection") {
		return true
	}
	return false
}

var (
	invalidGatewayStatus = []string{
		strings.ToLower(http.StatusText(http.StatusBadGateway)),
		strings.ToLower(http.StatusText(http.StatusGatewayTimeout)),
		strings.ToLower(http.StatusText(http.StatusBadRequest)),
		strings.ToLower(http.StatusText(http.StatusProxyAuthRequired)),
	}
)

// IsGatewayIssue returns a boolean indicating whether the error is related to known gateway issues.
func IsGatewayIssue(err error) bool {
	msg := strings.ToLower(err.Error())
	for _, status := range invalidGatewayStatus {
		if strings.Contains(msg, status) {
			return true
		}
	}
	return false
}
