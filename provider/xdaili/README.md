# xdaili [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/xdaili)

Package xdaili provides a custom http.Transport and helpers for dynamic proxy service powered by xdaili.cn .

## install

```bash
go get -d bitbucket.org/ai69/xdaili
```
