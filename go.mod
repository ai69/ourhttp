module bitbucket.org/ai69/ourhttp

go 1.16

require (
	bitbucket.org/ai69/popua v0.0.9
	bitbucket.org/ai69/zapcfg v0.2.1
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/avast/retry-go v3.0.0+incompatible
	go.uber.org/zap v1.19.1
)
