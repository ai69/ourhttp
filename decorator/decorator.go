package decorator

import "net/http"

// Decorator is a convenience function to represent our middleware inner function.
type Decorator func(http.RoundTripper) http.RoundTripper

// Decorate is a helper to wrap all the middleware.
func Decorate(t http.RoundTripper, rts ...Decorator) http.RoundTripper {
	decorated := t
	for i := len(rts) - 1; i >= 0; i-- {
		if rts[i] != nil {
			decorated = rts[i](decorated)
		}
	}
	return decorated
}

// TransportFunc implements the RoundTripper interface.
type TransportFunc func(*http.Request) (*http.Response, error)

// RoundTrip just calls the original function.
func (tf TransportFunc) RoundTrip(r *http.Request) (*http.Response, error) {
	return tf(r)
}
