package preset

import (
	"net/http"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	mw "bitbucket.org/ai69/ourhttp/middleware"
	"go.uber.org/zap"
)

// GetHTTPClientForDebug returns a HTTP client that dumps requests and responses in folder log.
func GetHTTPClientForDebug(log *zap.Logger) *http.Client {
	transport := decorator.Decorate(
		http.DefaultTransport.(*http.Transport),
		mw.ZapVerboseLogger(log),
		mw.RetryOnTimeout(5, 60*time.Second),
		mw.UserAgent(),
		mw.DumpRequest("log", "dump"),
		mw.DumpResponse("log", "dump"),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}
