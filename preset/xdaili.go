package preset

import (
	"crypto/tls"
	"net"
	"net/http"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	mw "bitbucket.org/ai69/ourhttp/middleware"
	"bitbucket.org/ai69/ourhttp/provider/xdaili"
)

func getXdailiTransport() *http.Transport {
	upstream := &http.Transport{
		Proxy: xdaili.GetProxyURL(),
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	}
	return upstream
}

// GetAuthHTTPClientWithXdailiProxy returns a HTTP client with TokenAuth + UserAgent + XdailiProxy middlewares.
func GetAuthHTTPClientWithXdailiProxy(proxyOrder, proxySecret, token string) *http.Client {
	transport := decorator.Decorate(
		getXdailiTransport(),
		mw.RetryOnBadProxy(6),
		mw.RetryOnTimeout(6, 90*time.Second),
		mw.TokenAuth(token),
		mw.UserAgent(),
		mw.XdailiProxyAuth(proxyOrder, proxySecret),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}

// GetHTTPClientWithXdailiProxy returns a HTTP client with UserAgent + XdailiProxy middlewares.
func GetHTTPClientWithXdailiProxy(proxyOrder, proxySecret string) *http.Client {
	transport := decorator.Decorate(
		getXdailiTransport(),
		mw.RetryOnBadProxy(6),
		mw.RetryOnTimeout(5, 60*time.Second),
		mw.UserAgent(),
		mw.XdailiProxyAuth(proxyOrder, proxySecret),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}

// GetAuthHTTPClient returns a HTTP client with TokenAuth + UserAgent middlewares.
func GetAuthHTTPClient(token string) *http.Client {
	transport := decorator.Decorate(
		http.DefaultTransport.(*http.Transport),
		mw.RetryOnTimeout(3, 60*time.Second),
		mw.TokenAuth(token),
		mw.UserAgent(),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}
