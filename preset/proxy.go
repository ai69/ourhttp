package preset

import (
	"crypto/tls"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	mw "bitbucket.org/ai69/ourhttp/middleware"
)

// getProxyTransport returns a cloned http.Transport with proxy and disable TLS verification.
func getProxyTransport(urlRaw string) *http.Transport {
	pu, err := url.Parse(urlRaw)
	if err != nil {
		panic(err)
	}
	upstream := http.DefaultTransport.(*http.Transport).Clone()
	upstream.Proxy = http.ProxyURL(pu)
	upstream.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	return upstream
}

// GetAuthHTTPClientWithProxy returns a HTTP client with TokenAuth + UserAgent + Proxy middlewares.
func GetAuthHTTPClientWithProxy(proxyURL, token string) *http.Client {
	transport := decorator.Decorate(
		getProxyTransport(proxyURL),
		mw.RetryOnBadProxy(6),
		mw.RetryOnTimeout(6, 90*time.Second),
		mw.TokenAuth(token),
		mw.UserAgent(),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}

// GetHTTPClientWithProxy returns a HTTP client with UserAgent + Proxy middlewares.
func GetHTTPClientWithProxy(proxyURL string) *http.Client {
	transport := decorator.Decorate(
		getProxyTransport(proxyURL),
		mw.RetryOnBadProxy(6),
		mw.RetryOnTimeout(5, 60*time.Second),
		mw.UserAgent(),
	)
	client := &http.Client{
		Transport: transport,
	}
	return client
}
