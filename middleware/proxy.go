package middleware

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	"bitbucket.org/ai69/ourhttp/helper"
	xd "bitbucket.org/ai69/ourhttp/provider/xdaili"
	rg "github.com/avast/retry-go"
	"go.uber.org/zap"
)

// XdailiProxyAuth is a middleware decorator for authentication of xdaili.
func XdailiProxyAuth(order, secret string) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			req.Header.Set(xd.GetAuthHeader(order, secret))
			resp, err := c.RoundTrip(req)
			return resp, err
		})
	}
}

// RetryOnBadProxy is a middleware decorator to retry with backoff + random for gateway issues.
func RetryOnBadProxy(maxTimes uint) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (resp *http.Response, err error) {
			log := glog.With(zap.String("debug_midware", "proxy"))
			if req != nil {
				log = log.With(zap.String("request_url", req.URL.String()), zap.String("request_method", req.Method))
			}

			lastErr := rg.Do(
				func() error {
					resp, err = c.RoundTrip(req)
					if err == nil && resp != nil && (resp.StatusCode == http.StatusBadGateway ||
						resp.StatusCode == http.StatusGatewayTimeout ||
						resp.StatusCode == http.StatusBadRequest) {
						err = fmt.Errorf("error status: %v", resp.Status)
						log.Warn("got error status code", zap.Error(err))
					}
					return err
				},
				rg.RetryIf(func(err error) bool {
					if helper.IsGatewayIssue(err) {
						log.Debug("retry for bad gateway", zap.Error(err))
						return true
					}
					log.Debug("log error without retry", zap.Error(err))
					return false
				}),
				rg.Attempts(maxTimes),
				rg.Delay(300*time.Millisecond),
				rg.MaxDelay(10*time.Second),
				rg.MaxJitter(2*time.Second),
				rg.LastErrorOnly(true),
			)

			if resp != nil {
				log = log.With(zap.String("status", resp.Status))
			}
			log.Debug("got final result of proxy-retry", zap.Error(lastErr))
			return resp, err
		})
	}
}
