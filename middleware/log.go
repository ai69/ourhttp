package middleware

import (
	"os"

	zc "bitbucket.org/ai69/zapcfg"
	"go.uber.org/zap"
)

var glog *zap.Logger

func init() {
	LogOff()
}

func LogOn() {
	glog = zc.GetDevelopmentLogger("", "").With(zap.Int("pid", os.Getpid()))
}

func LogOff() {
	glog = zc.GetNoopLogger()
}

func LogSet(l *zap.Logger) {
	glog = l
}
