package middleware

import (
	"net/http"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	"go.uber.org/zap"
)

func ZapVerboseLogger(l *zap.Logger) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			start := time.Now()
			resp, err := c.RoundTrip(req)
			log := l.With(zap.Duration("time_cost", time.Since(start)))

			if req != nil {
				log = log.With(zap.String("request_url", req.URL.String()), zap.String("request_method", req.Method))
			}
			if resp != nil {
				log = log.With(zap.Int("status_code", resp.StatusCode), zap.String("status_str", resp.Status))
			}

			if err != nil {
				log.Warn("got response error", zap.Error(err))
			} else if resp == nil {
				log.Warn("got empty response")
			} else if resp.StatusCode < 200 || resp.StatusCode > 399 {
				log.Warn("got non-okay status")
			} else {
				log.Info("got okay status")
			}
			return resp, err
		})
	}
}
