package middleware

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	"github.com/1set/gut/yos"
	yr "github.com/1set/gut/yrand"
	ys "github.com/1set/gut/ystring"
	"go.uber.org/zap"
)

func ensureOutputDirectory(dir ...string) string {
	const defaultDir = "dump"
	name := yos.JoinPath(dir...)
	if ys.IsBlank(name) {
		name = defaultDir
	}
	if err := yos.MakeDir(name); err != nil {
		log := glog.With(zap.String("debug_midware", "dump"))
		log.Warn("fail to create output directory", zap.String("dir", name))
	}
	return name
}

func saveDumpFile(dir, ext string, data []byte) {
	tag, _ := yr.StringBase36(6)
	path := yos.JoinPath(dir, fmt.Sprintf("%s-%s.%s", time.Now().UTC().Format("20060102150405"), tag, ext))
	_ = ioutil.WriteFile(path, data, 0644)
}

func DumpRequest(outDir ...string) decorator.Decorator {
	outPath := ensureOutputDirectory(outDir...)
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			if req != nil {
				if dump, ed := httputil.DumpRequest(req, true); ed == nil {
					saveDumpFile(outPath, "req", dump)
				}
			}
			resp, err := c.RoundTrip(req)
			return resp, err
		})
	}
}

func DumpResponse(outDir ...string) decorator.Decorator {
	outPath := ensureOutputDirectory(outDir...)
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			resp, err := c.RoundTrip(req)
			if resp != nil {
				if dump, ed := httputil.DumpResponse(resp, true); ed == nil {
					saveDumpFile(outPath, "resp", dump)
				}
			}
			return resp, err
		})
	}
}
