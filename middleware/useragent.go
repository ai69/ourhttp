package middleware

import (
	"context"
	"net/http"

	"bitbucket.org/ai69/ourhttp/decorator"
	pua "bitbucket.org/ai69/popua"
	ys "github.com/1set/gut/ystring"
)

const (
	UserAgentRandomSeed = "ourhttp-user-agent-seed"
)

func getUserAgent(ctx context.Context) (ua string) {
	if ctx != nil {
		if valRaw := ctx.Value(UserAgentRandomSeed); valRaw != nil {
			if valStr, ok := valRaw.(string); ok {
				ua = pua.GetWeightedBySeed(valStr)
			}
		}
	}
	if ys.IsBlank(ua) {
		ua = pua.GetWeightedRandom()
	}
	return ua
}

// UserAgent is a middleware decorator to add random User-Agent in the header.
// In order to get the consistent user agent for various requests, you may set random seed
// as a string value in the request context with `middleware.UserAgentRandomSeed` as the key.
func UserAgent() decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			req.Header.Set("User-Agent", getUserAgent(req.Context()))
			resp, err := c.RoundTrip(req)
			return resp, err
		})
	}
}
