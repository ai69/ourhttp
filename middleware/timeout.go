package middleware

import (
	"context"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/ai69/ourhttp/decorator"
	"bitbucket.org/ai69/ourhttp/helper"
	rg "github.com/avast/retry-go"
	"go.uber.org/zap"
)

// RetryOnTimeout is a middleware decorator to retry on request timeout.
func RetryOnTimeout(maxTimes uint, timeout time.Duration) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (resp *http.Response, err error) {
			log := glog.With(zap.String("debug_midware", "timeout"))
			if req != nil {
				log = log.With(zap.String("request_url", req.URL.String()), zap.String("request_method", req.Method))
			}

			lastErr := rg.Do(
				func() error {
					var ctx context.Context
					if req.Context() != nil {
						ctx = req.Context()
					} else {
						ctx = context.Background()
					}
					ctx, _ = context.WithTimeout(ctx, timeout)
					// defer cancel()

					resp, err = c.RoundTrip(req.WithContext(ctx))
					return err
				},
				rg.RetryIf(func(err error) bool {
					if helper.IsTimeout(err) {
						log.Debug("retry for network timeout", zap.Error(err))
						return true
					} else if strings.Contains(err.Error(), "request canceled") ||
						strings.Contains(err.Error(), "request canceled while waiting for connection") ||
						strings.Contains(err.Error(), "timeout awaiting response headers") {
						log.Debug("retry for connection timeout", zap.Error(err))
						return true
					} else {
						log.Debug("log error without retry", zap.Error(err))
						return false
					}
				}),
				rg.Attempts(maxTimes),
				rg.Delay(300*time.Millisecond),
				rg.MaxDelay(timeout/3),
				rg.MaxJitter(2*time.Second),
				rg.LastErrorOnly(true),
			)

			if resp != nil {
				log = log.With(zap.String("status", resp.Status))
			}
			log.Debug("got final result of timeout-retry", zap.Error(lastErr))
			return resp, err
		})
	}
}
