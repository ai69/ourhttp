package middleware

import (
	"net/http"

	"bitbucket.org/ai69/ourhttp/decorator"
)

// BasicAuth is a middleware decorator for basic access authentication with username and password.
func BasicAuth(username, password string) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			req.SetBasicAuth(username, password)
			resp, err := c.RoundTrip(req)
			return resp, err
		})
	}
}

// TokenAuth is a middleware decorator for basic access authentication with token.
func TokenAuth(token string) decorator.Decorator {
	return func(c http.RoundTripper) http.RoundTripper {
		return decorator.TransportFunc(func(req *http.Request) (*http.Response, error) {
			req.Header.Set("Authorization", "token "+token)
			resp, err := c.RoundTrip(req)
			return resp, err
		})
	}
}
