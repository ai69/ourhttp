# ourhttp [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/ourhttp)

**ourhttp** is a collection of packages that provides a decorator framework and middlewares to customize HTTP clients.

## install

```bash
go get -d bitbucket.org/ai69/ourhttp
```

## packages

- [![go.dev decorator](https://img.shields.io/badge/go.dev-decorator-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/ourhttp/decorator)
- [![go.dev middleware](https://img.shields.io/badge/go.dev-middleware-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/ourhttp/middleware)
- [![go.dev helper](https://img.shields.io/badge/go.dev-helper-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/ourhttp/helper)
- [![go.dev preset](https://img.shields.io/badge/go.dev-preset-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/ourhttp/preset)
